import java.util.Arrays; 
import java.util.Comparator; 
public class ProgramacaoGulosa { 
     static int max(int a, int b) { return (a > b)? a : b; } 
       
     static int Mochila(int tamanho, int valor[], int peso[], int n) 
    { 
		if (n == 0 || tamanho == 0) 
			return 0; 
       
		if (valor[n-1] > tamanho) 
			return Mochila(tamanho, valor, peso, n-1); 
       
		else return max( peso[n-1] + Mochila(tamanho-valor[n-1], valor, peso, n-1), 
                     Mochila(tamanho, valor, peso, n-1) 
						); 
     } 
  
	public static void main(String args[]) 
	{ 
		int peso[] = new int[]{13, 23, 17, 19, 22, 37, 7, 25}; 
        int valor[] = new int[]{23, 29, 27, 25, 21, 42, 11, 99}; 
		int  tamanho = 50; 
		int n = peso.length; 
		System.out.println(Mochila(tamanho, valor, peso, n)); 
    } 
}