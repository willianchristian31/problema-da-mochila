import java.io.*;
import java.util.*;
public class ProgramacaoDinamica {
	public static void main(String[] args) {
        BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		int w = 0, n = 0;
        	try {
				
        	String arr [] = new String [2];
        	arr = inReader.readLine().split(" "); 
        	n = Integer.parseInt(arr[0]);
        	w = Integer.parseInt(arr[1]);

        	int val[] = new int[n], 
        		wt[] = new int[n];

        	arr = new String[n];
           	 arr = inReader.readLine().split(" ");
            	for (int i = 0; i < arr.length; i++) 
            		wt[i] = Integer.parseInt(arr[i]);

            	arr = new String[w];
            	arr = inReader.readLine().split(" ");
            	for (int i = 0; i < arr.length; i++) 
            		val[i] = Integer.parseInt(arr[i]);

            	inReader.close();

            	System.out.println("Valor: "+adicionaMochila(val, wt, w));

        	} catch (IOException e) {
            		System.err.println(e.getMessage());
        	}
	}

	private static int adicionaMochila(int val[], int wItem[], int wMochila){

		int n = wItem.length;
		int [][] m = new int[n + 1][wMochila + 1]; 
		for (int item = 1; item <= n; item++) {
			for (int mochilaTamanho = 1; mochilaTamanho <= wMochila; mochilaTamanho++ ) {
				if (wItem[item - 1] <= mochilaTamanho)
					m [item][mochilaTamanho] = Math.max(val[item - 1] + m[item - 1][ mochilaTamanho-wItem[item - 1] ], 
										m[item - 1][mochilaTamanho]);
				else
					m [item][mochilaTamanho] = m[item - 1][mochilaTamanho];
			}
		}
		ArrayList<Integer> itensSelecionados = new ArrayList<Integer>();
		int colMochilaTamanho = wMochila;
		for (int item = n; item >= 1; item--) {
			if (m [item][colMochilaTamanho] != m [item - 1][colMochilaTamanho]){
				itensSelecionados.add(item);
				colMochilaTamanho -= wItem[item - 1];
			}
		}

		System.out.println("Produtos escolhidos: "+itensSelecionados);

		return m[n][wMochila];
	}
}